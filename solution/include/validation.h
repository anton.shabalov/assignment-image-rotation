//
// Created by antonsabalov on 12/26/21.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_VALIDATION_H
#define ASSIGNMENT_IMAGE_ROTATION_VALIDATION_H
#include "imagesUtil.h"



enum read_status chechArg(int argc);
enum read_status checkNameFile(char *name);
enum read_status checkReadFile(FILE *fileIn);
enum read_status cheImages(struct image *img);
#endif //ASSIGNMENT_IMAGE_ROTATION_VALIDATION_H
