//
// Created by antonsabalov on 12/24/21.
//

#ifndef _IMAGES_UTIL_H_
#define _IMAGES_UTIL_H_

#include <stdint.h>
#include <stdio.h>

#include "util.h"

enum read_status  {
    READ_OK = 1,
    READ_INVALID_SIGNATURE=0,
    READ_INVALID_BITS=0,
    READ_INVALID_HEADER=0,
    READ_INVALID_ARGS=0,
    not_invalid_Signature_BMP_file=0,
    READ_INVALID=0,
    FILE_INVALID=0
    /* коды других ошибок  */
};
enum  write_status  {
    WRITE_OK = 1,
    WRITE_ERROR
    /* коды других ошибок  */
};
    struct __attribute__((packed)) bmp_header
    {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
    };

void setParam(struct image *img,uint32_t width, uint32_t height);
enum write_status to_bmp(FILE *fileOut, struct image *img);
enum read_status from_bmp(FILE *fileIn, struct image *img);
enum read_status makehead(uint32_t width, uint32_t height, struct bmp_header *header);
void free_image(struct image* image);

#endif //_IMAGES_UTIL_H_
