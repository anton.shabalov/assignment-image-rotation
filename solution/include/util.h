//
// Created by antonsabalov on 12/24/21.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_UTIL_H
#define ASSIGNMENT_IMAGE_ROTATION_UTIL_H
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image {
    uint32_t width, height;
    struct pixel *data;
};
struct pixel {
    uint8_t b, g, r;
};
bool mallocs(struct image *img);
struct image transform(struct image const images);
struct image makeImg(uint32_t width, uint32_t height);
void writeFile(char *name,FILE **file);
void readfile(char *name,FILE **file);
void filelose(FILE *file);


#endif //ASSIGNMENT_IMAGE_ROTATION_UTIL_H
