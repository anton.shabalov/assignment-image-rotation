//
// Created by antonsabalov on 12/24/21.
//

#include "../include/util.h"

#include <stdlib.h>

struct image transform(struct image const images){

    struct image newImg= makeImg(images.width,images.height);
    struct pixel *newImages = malloc(sizeof(struct pixel) * newImg.height * newImg.width);
    if(!newImages){
        newImg.data=NULL;
        return newImg;
    }
    size_t x=0;
    size_t y=0;

    while(x<images.height){

        while(y<images.width){
            newImages[images.height * y + (images.height - 1 - x)] = images.data[x * images.width + y];

            y++;

        }
        y=0;
        x++;
    }
    newImg.data = newImages;
    return newImg;
}

bool mallocs(struct image *img){
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    if(img->data==NULL){
        return false;
    }else{
        return true;
    }
}
struct image makeImg(uint32_t width, uint32_t height){
    struct image newImg;
    newImg.height=width;
    newImg.width=height;
    return newImg;
}
void readfile(char *name,FILE **file) {
        *file = fopen(name, "rb");


}
void writeFile(char *name,FILE **file) {
        *file = fopen(name, "wb");
}

void filelose(FILE *file){
    fclose(file);
}


