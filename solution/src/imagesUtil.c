//
// Created by antonsabalov on 12/24/21.
//

#include "imagesUtil.h"

#include <inttypes.h>
#include <stdlib.h>

#include "util.h"

#define bfTypes 0x4D42
#define zoros 0x0
#define offBits 0x36
#define Sizes 0x28
#define Plan 0x1
#define biBitCounts 0x18

enum read_status makehead(uint32_t width, uint32_t height, struct bmp_header *header) {

    size_t padding =(4 - width * 3%4) % 4;
    size_t bffiles=(width * sizeof(struct pixel) + padding) * height;
    if(bffiles==0){
        return READ_INVALID_HEADER;
    }
    header->bfType = bfTypes;
            header->bfileSize = bffiles + sizeof(struct bmp_header);
            header->bfReserved = zoros;
            header->bOffBits = offBits;
            header->biSize = Sizes;
            header->biWidth = width,
            header->biHeight = height,
            header->biPlanes = Plan;
            header->biBitCount = biBitCounts;
            header->biCompression = zoros;
            header->biSizeImage = bffiles;
            header->biXPelsPerMeter = zoros;
            header->biYPelsPerMeter = zoros;
            header->biClrUsed = zoros;
            header->biClrImportant = zoros;
            return READ_OK;

}

static bool read_data(uint32_t width, uint32_t height, struct pixel* data, FILE* file) {
	int8_t padding = (4 - width * 3 % 4) % 4;
	size_t i = 0;
    bool flag=false;
    
	while (i < height) {
        if(fread(data + i * width, width * sizeof(struct pixel), 1, file)){
           if(fseek(file, padding, SEEK_CUR)){
                flag=true;
           }
            
            
        }else{
            fseek(file, padding, SEEK_CUR);
        }
	
    	++i;
    }
    return flag;
}

enum read_status from_bmp(FILE *fileIn, struct image *img) {
    struct bmp_header header={0};
    if(!fread(&header, sizeof(struct bmp_header), 1, fileIn)){
         free(img->data);
        return READ_INVALID;
    }

    setParam(img,header.biWidth, header.biHeight);
     size_t* z = (size_t*)mallocs(img);
    fseek(fileIn, header.bOffBits, SEEK_SET);
    if(z==0){
        free(img->data);
        return not_invalid_Signature_BMP_file;
    }
    if(!read_data(img->width, img->height, img->data, fileIn)){
        return FILE_INVALID;
        
    }
    free(z);

    return READ_OK;
}


void setParam(struct image *img,uint32_t width, uint32_t height){
    img->width = width;
    img->height = height;
}

static bool write_data(uint32_t width, uint32_t height, struct pixel* data, FILE* file) {
	int8_t padding = (4 - width * 3 % 4) % 4;
	uint32_t pad = 0;
    bool flag=false;

	size_t i = 0;

	while (i < height) {
        if(fwrite(data + i * width, width * sizeof(struct pixel), 1, file)&&fwrite(&pad, padding, 1, file) ){
            flag=true;
            
        }
		
		
	
    	++i;
    }
    return flag;
}

enum write_status to_bmp(FILE *fileOut, struct image *img) {
    struct bmp_header header={0};
    makehead(img->width, img->height, &header);
    size_t s=fwrite(&header, 1, sizeof(header), fileOut);
    if(s==0){
        return WRITE_ERROR;
    }
    if(!write_data(img->width, img->height, img->data, fileOut)){
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

void free_image(struct image *image) {
	if (image != NULL)
		free(image->data);
}









