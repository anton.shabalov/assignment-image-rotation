 //
// Created by antonsabalov on 12/24/21.
//
#include "../include/imagesUtil.h"
#include "../include/util.h"
#include "../include/validation.h"

int main(int argc, char **argv) {
    if(chechArg(argc)) {
        FILE *inputFile;
        FILE *outputFile;
        struct image img;
        struct image rotateImg;
        char *inp = argv[1];
        char *outp = argv[2];
        if(!(checkNameFile(inp)&& checkNameFile(outp))){
            return 0;
        }
        readfile(inp, &inputFile);
        if(!checkReadFile(inputFile)){
            return 0;
        }
        from_bmp(inputFile, &img);
          filelose(inputFile);
        if(cheImages(&img)){
            
            return 0;
        }
        rotateImg = transform(img);
        if(rotateImg.data==NULL){
            return 0;
        }
        free_image(&img);
        writeFile(outp, &outputFile);
        if(!checkReadFile(outputFile)){
            
            return 0;
        }
        if(cheImages(&rotateImg)){
           
            filelose(outputFile);
            return 0;
        }
        to_bmp(outputFile, &rotateImg);
		free_image(&rotateImg);
       
        filelose(outputFile);
	}else{
        return 0;
    }
}
