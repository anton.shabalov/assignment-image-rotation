//
// Created by antonsabalov on 12/26/21.
//

#include "validation.h"

#include <string.h>

#include "imagesUtil.h"

enum read_status cheImages(struct image *img){
    if (img==0){
        return READ_OK;
    } else{
        return READ_INVALID_ARGS;
    }
}

enum read_status chechArg(int argc){
    if(argc==3){
        fprintf(stderr, "считывание аргументов прошло успешно\n");
        return READ_OK;
    }else{
        fprintf(stderr, "Вы указали не верное количество аргументов\nВы должны указать путь к исходному файлу и путь к конечному файлу!\n");
        return READ_INVALID_ARGS;
    }

}
enum read_status checkNameFile(char *name){
    if(name==NULL||strlen(name)==0){
        fprintf(stderr, "Длина входного или выходного файла равна 0\n");
        return READ_INVALID_SIGNATURE;
    }else{
        return READ_OK;
    }


}

enum read_status checkReadFile(FILE *fileIn){
    if(fileIn==NULL){
        fprintf(stderr, "не удалось открыть файл для записи или чтения\n");
        return READ_INVALID_SIGNATURE;
    }else{
        return READ_OK;
    }

}
